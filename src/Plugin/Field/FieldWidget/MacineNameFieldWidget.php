<?php

namespace Drupal\machine_name_field_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 *
 * @FieldWidget(
 *   id = "machine_name_field_widget",
 *   label = @Translation("Machine Name"),
 *   field_types = {
 *     "string"
 *   },
 * )
 */
class MacineNameFieldWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'machine_name',
      '#default_value' => $value,
      '#machine_name' => [
        'replace_pattern' => '[^a-z0-9_]+',
        'replace' => '_',
      ],
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];
    return ['value' => $element];
  }

  /**
   * Validate the fields and convert them into a single value as text.
   */
  public static function validate($element, FormStateInterface $form_state, $context) {
    $value = $element['#value'];

    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

    // Verify that the machine name not start with any number.
    if (is_numeric($value[0])) {
      $form_state
        ->setError($element, t('The machine-readable name must start with letter.'));
    }

    // Verify that the machine name not only consists of replacement tokens.
    if (preg_match('@^' . $element['#machine_name']['replace'] . '+$@', $element['#value'])) {
      $form_state
        ->setError($element, t('The machine-readable name must contain unique characters.'));
    }

    // Verify that the machine name contains no disallowed characters.
    if (preg_match('@' . $element['#machine_name']['replace_pattern'] . '@', $value)) {
      if (!isset($element['#machine_name']['error'])) {

        // Since a hyphen is the most common alternative replacement character,
        // a corresponding validation error message is supported here.
        if ($element['#machine_name']['replace'] == '-') {
          $form_state
            ->setError($element, t('The machine-readable name must contain only lowercase letters, numbers, and hyphens.'));
        }
        else {
          $form_state
            ->setError($element, t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
        }
      }
      else {
        $form_state
          ->setError($element, $element['#machine_name']['error']);
      }
    }
  }

}
