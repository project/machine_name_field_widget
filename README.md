CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

-- INTRODUCTION --

* This module basically provides a widget by UI of string field (textfield)
for a machine name validation purpose for example prevent from start
with number and not allowed for space and other validation.

-- REQUIREMENTS --
* Enabled core module: Field and Text.

-- INSTALLATION --
* Install as usual, see:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

-- Maintainers --
Current maintainers:
* Mohammed Abdullah Bamlhes (bamlhes) - https://www.drupal.org/u/bamlhes
* Abdulaziz zaid (abdulaziz1zaid) - https://www.drupal.org/u/abdulaziz1zaid
